Network Automation Coding Challenge
---

## DOCKER
# build app
sudo docker build -t {app_name} .
# run app
sudo docker run -p {out_port}:8000 {app_name} poetry run python app.py

## DOCKER-COMPOSE
# run app
sudo docker-compose up
