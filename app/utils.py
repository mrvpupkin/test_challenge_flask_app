import os
import ipaddress


class DataHandler:
    """DB data handler"""
    def get_fake_db_data(self, get_full_data=False):
        root_dir = os.path.dirname(os.path.abspath(__file__))
        res = []
        with open(f'{root_dir}/db_data.csv', 'r') as f:
            data_list = f.readlines()
            fields_names = []
            for  i, line in enumerate(data_list):
                line = line.replace('\n', '').split(',')
                if i == 0:
                    fields_names = line
                    continue
                else:
                    record = dict(zip(fields_names, line))
                    if not get_full_data:
                        record.pop("created_date", None)
                        record.pop("created_by", None)
                    res.append(record)
        return res


class IpHandler:
    """IP handler"""

    @staticmethod
    def get_ip_addr(ip):
        try:
            ip_addr = ipaddress.ip_address(ip)
        except ValueError as e:
            # wrong ip address format in params!
            ip_addr = None
            print(e)
        return ip_addr

    @staticmethod
    def get_ip_addr_network(ip, mask):
        try:
            ip_network = ipaddress.ip_network(f"{ip}/{mask}", False)
            print(f"{ip_network=}")
        except ValueError as e:
            # wrong mask address format in params!
            ip_network = []
            print(e)
        return ip_network
