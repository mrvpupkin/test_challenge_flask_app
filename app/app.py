from flask import Flask, render_template, request, session
from flask.views import MethodView
from flask_restx import Resource, Api

from utils import DataHandler, IpHandler

app = Flask(__name__)
api = Api(app)



@api.route('/addresses')
@api.doc(params={'addr': {'description': 'IP address'},
                 'mask': {'description': 'IP network mask'}})
class AddressesView(Resource):
    """returns JSON format"""
    db_handler = DataHandler()

    def get(self):
        req_params = dict(request.args)
        addr = req_params.get("addr")
        mask = req_params.get("mask")

        data = self.db_handler.get_fake_db_data()
        if addr and mask:
            # make search
            ip_addr = IpHandler.get_ip_addr(addr)
            if not ip_addr:
                data = []
            else:
                ip_networkds = []
                for row in data:
                    ip_networkds.append({"id": row["id"], "address_network": IpHandler.get_ip_addr_network(row["address"], mask)})
                filtered_rows = list(filter(lambda x: ip_addr in x["address_network"], ip_networkds))
                filtered_ids = list(map(lambda x: x["id"], filtered_rows))
                data = list(filter(lambda x: x["id"] in filtered_ids, data))
        return data


class ReportView(MethodView):
    """returns HTML format"""
    db_handler = DataHandler()

    def get(self):
        req_params = dict(request.args)
        addr = req_params.get("addr")
        mask = req_params.get("mask")

        # getting data
        # TODO: optimize for big data
        data = self.db_handler.get_fake_db_data()

        # get fields names for html table
        # TODO: optimize for big data (retrieve fields names without getting data first)
        fields_names = []
        if data:
            fields_names = list(data[0].keys())

        if addr and mask:
            # make search
            ip_addr = IpHandler.get_ip_addr(addr)
            if not ip_addr:
                data = []
            else:
                ip_networkds = []
                for row in data:
                    ip_networkds.append({"id": row["id"], "address_network": IpHandler.get_ip_addr_network(row["address"], mask)})
                filtered_rows = list(filter(lambda x: ip_addr in x["address_network"], ip_networkds))
                filtered_ids = list(map(lambda x: x["id"], filtered_rows))
                data = list(filter(lambda x: x["id"] in filtered_ids, data))

        context = {"data": data, "fields": fields_names}
        return render_template("report.html", context=context)


app.add_url_rule('/report', view_func=ReportView.as_view('report'))



if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0', port=8000)
