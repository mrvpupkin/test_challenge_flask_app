FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
EXPOSE 8000

# sys packages
RUN apt update \
    && apt upgrade -y \
    && apt install -y wget \
    && apt install -y python3 \
    && apt install -y python3-pip
ENV PYTHONIOENCODING=utf-8

# poetry
ENV POETRY_HOME=/opt/poetry
RUN wget -qO- https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 - \
    && chmod o+x /opt/poetry/bin/poetry

# user & workdir
RUN useradd -m container_user -u1000 && mkdir /app && chown -R container_user:container_user app

WORKDIR /app
ADD pyproject.toml /app
ADD poetry.lock /app
COPY app/ /app
RUN chown -R container_user *

USER container_user

# virtualenv & requirements install (if not poetry)
# RUN ...virtualenv creation...
# RUN pip install -r requirements.txt

# env packages
ENV PATH="/opt/poetry/bin:$PATH"
RUN poetry install

# RUN poetry run python app.py
# CMD ["poetry", "run", "python", "app.py"]
